#!/bin/bash
for tf in 2.8.0
do
    for cv in 4.5.5
    do
        echo "Building OpenCV ${cv} in Tensorflow ${tf}"
        docker build --progress plain --force-rm  --rm . -t unmonoqueteclea/tensorflow-opencv:tf${tf}-cv${cv}  \
               --build-arg OPENCV_VERSION=$cv \
               --build-arg TF_VERSION=$tf
    done
done

      
