#!/bin/bash
for py in 3.9 3.10
do
    for cv in 4.5.5
    do
        echo "Building OpenCV ${cv} in Python ${py}"
        docker build --progress plain --force-rm  --rm . -t unmonoqueteclea/python-opencv:py${py}-cv${cv}  \
               --build-arg OPENCV_VERSION=$cv \
               --build-arg PYTHON_VERSION=$py
    done
done

      
